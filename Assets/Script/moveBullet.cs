﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBullet : MonoBehaviour
{
    public float speed;

    void Start()
    {
        Destroy (this.gameObject, 0.5f);
    }

    void Update()
    {
        transform.Translate(Vector3.up * speed);
    }
}
