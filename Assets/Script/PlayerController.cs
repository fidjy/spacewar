﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  
    public Rigidbody2D player;
    public float speed;
    public GameObject Bolt;
    public Transform shotSpawn;
    public Transform shotSpawn1;

    void Start()
    {
        player = GetComponent<Rigidbody2D>();
        
    }

    private void Update()
    {
        float x = Input.GetAxis ("Horizontal") * speed;
        float y = Input.GetAxis ("Vertical") * speed;
        player.velocity = new Vector2(x, y);
        
    if(Input.GetButtonDown("Fire1"))
    {
        Instantiate(Bolt, shotSpawn.position, shotSpawn.rotation);
        Instantiate(Bolt, shotSpawn1.position, shotSpawn1.rotation);
    }
    }
}
