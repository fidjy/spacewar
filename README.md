# SpaceWar

## Contexte
### Le projet

Le projet consiste à réaliser un jeu de tir en coopération jusqu’à deux joueurs. Leur objectif sera de tuer un boss après avoir affronté plusieurs vagues d’ennemies.

### Le thème

Le jeu se déroulera dans un univers futuriste ou une guerre fait rage entre deux factions. Les protagonistes évolueront dans l’espace où ils devront faire face, après la destruction quasi complète de leur flotte, à plusieurs vagues d’ennemies pour défendre leur planète.

## Objectif

### V1
- Créer une GUI de navigation
```mermaid
graph TD;
A[Space-War]-->B[Campagne];
A-->C[Scores];
A-->D[Options];
A-->E[quitter];
B-->BA[Level1];
B-->BB[Level2];
B-->BC[Level3];
B-->BD[Level4];
B-->BE[Level5];
D-->DA[Son];
D-->DB[Difficulter];
D-->DC[Reset score];
```

- Créer un jeu qui comporte les élément suivant
```mermaid
graph TD;
A[Space-War]-->B[Joueur];
A-->C[Ennemies];
A-->D[Carte];
B-->BA[Déplacement];
B-->BB[Tir];
B-->BC[Point de vie];
B-->BD[Score];
C-->CA[Type];
D-->DA[Taille];
D-->DB[Texture];
BA-->BAA[Vitesse];
BA-->BAB[Direction];
BB-->BBA[Dégat];
BB-->BBB[Vitesse];
BB-->BBC[Délais];
CA-->CAA[Déplacement];
CA-->CAB[Tir];
CA-->CAC[Point de vie];
CAA-->CAAA[Vitesse];
CAA-->CAAB[Direction];
CAB-->CABA[Dégat];
CAB-->CABB[Vitesse];
CAB-->CABC[Délais];
```

## Crédit

### MusiquE

- Menu
Auteur : Vendalist Prod
Titre : Rap/Hip-Hop Instrumental #10
https://soundcloud.com/vandalist-prod/instru-10-free-download